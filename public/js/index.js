class Recipes {
    constructor(known, index, name, description, spec, lvl, materials, specMats, time, tools, difficult, price, assist, malus, type) {
        this.name = name,
            this.description = description,
            this.spec = spec,
            this.lvl = lvl,
            this.materials = materials,
            this.time = time,
            this.tools = tools,
            this.difficult = difficult,
            this.price = price,
            this.assist = assist,
            this.malus = malus,
            this.known = known,
            this.specMats = specMats,
            this.index = index,
            this.type = type
    }
}

class Equipments extends Recipes {
    constructor(known, index, name, description, spec, lvl, carac, materials, specMats, time, tools, difficult, price, assist, malus, type) {
        super(known, index, name, description, spec, lvl, carac, materials, specMats, time, tools, difficult, price, assist, malus, type)
        this.description = description,
            this.spec = spec,
            this.lvl = lvl,
            this.materials = materials,
            this.time = time,
            this.tools = tools,
            this.difficult = difficult,
            this.price = price,
            this.assist = assist,
            this.malus = malus,
            this.carac = carac,
            this.known = known,
            this.specMats = specMats,
            this.index = index,
            this.type = type
    }
}

const myRecipes = {
    lvl1: [
        new Recipes(true,
            1,
            "Échelle en bois",
            "Vous aurez parfois besoin de fabriquer une échelle avec les moyens du bord. Ce n’est pas bien difficile mais il faut qu’elle soit solide.",
            "ébéniste, mécanicien, gobelin",
            "1",
            "(pour 1m) 1 unité de bois brut, 12 petits clous, 5 mètres de ficelle",
            "",
            "(pour 1m) 10 minutes",
            "scie à main, marteau, canif",
            "8(+3XP en cas de réussite)",
            "",
            "se faire aider par un assistant (AD supérieure à 10) donne un bonus de +2 et réduit le temps de 30%",
            "-4",
            "Objet"),
        new Recipes(true,
            2,
            "Chaise de base",
            "Exemple de fabrication d’un objet commun. On doit pouvoir s’asseoir dessus",
            "ébéniste, mécanicien, gobelin",
            "1",
            "1 unité de bois brut, 15 petits clous",
            "",
            "1 heure",
            "scie à main, marteau, canif",
            "7(+2XP en cas de réussite)",
            "de fabrication : 1 PO – de vente au public : 2 ou 3 PO",
            "se faire aider par un assistant (AD supérieure à 10) donne un bonus de +2 et réduit le temps de 30%",
            "-5",
            "Objet"),
        new Equipments(true,
            3,
            "Arc court de basse qualité",
            "Une arme de jet de base, conçue pour débuter, difficile à manier.",
            "ébéniste, mécanicien, gobelin",
            "1",
            "arme de jet, 1D+2 P.I., AD-2 (au tir de flèche), CHA-1, rupture 1-5",
            "1 unité de bois brut, ½ d’unité de cuir souple de base, 3 mètres de ficelle",
            "",
            "3 heures",
            "scie à main, canif, râpe à bois",
            "7(+4XP en cas de réussite)",
            "de fabrication : 3 PO – de négoce : 10 PO – de vente au public : 30 PO",
            "",
            "-4",
            "Équipement"),
        new Equipments(true,
            4,
            "Arc long de basse qualité",
            "Une arme de jet de base, conçue pour débuter, difficile à manier.",
            "ébéniste, mécanicien",
            "1",
            "arme de jet, 1D+3 P.I., AD-2 (au tir de flèche), CHA-1, rupture 1-5",
            "1 unité de bois brut, ½ d’unité de cuir souple de base",
            "1 corde à arc (au bazar, 3 PO)",
            "3 heures",
            "scie à main, canif, râpe à bois",
            "8(+5XP en cas de réussite)",
            "de fabrication : 5 PO – de négoce : 15 PO – de vente au public : 50 PO",
            "",
            "-4",
            "Équipement"),
        new Equipments(true,
            5,
            "Bouclier de base",
            "Un bouclier qui vous protège un peu et augmente le score de parade. Il n’est pas très pratique pour le reste.",
            "ébéniste, mécanicien, gobelin",
            "1",
            " +1 au score de PR, PRD+1, AT-2, AD-1, rup. 1-4",
            "2 unités de bois brut, 1 unité de cuir épais de base, 20 petits clous",
            "",
            "2 heures",
            "scie à main, canif, paire de cisailles, marteau",
            "6(+3XP en cas de réussite)",
            "de fabrication : 4 PO – de négoce : 10 PO – de vente au public : 30 PO",
            "",
            "-4",
            "Équipement"),
        new Equipments(true,
            2,
            "Affûtage de lame",
            "Technique d’affûtage efficace qui améliore les armes tranchantes pendant un temps limité.",
            " forgeron concepteur, mécanicien, gobelin",
            "1",

            "+1 aux dégâts pendant 3 combats (arme de base ou correcte) ou 5 combats (arme de qualité ou autre)",
            "1 arme tranchante (ni enchantée, ni « relique »)",
            "",
            "15 minutes",
            "nécessaire d’entretien du métal",
            "6(+2XP en cas de réussite)",
            "en tant que service, de 1 PO à 5 PO selon l’arme",
            "",
            "-4",
            "Équipement"),
        new Equipments(true,
            2,
            "Renforcement d’arme de base",
            "Le forgeron peut ici améliorer la résistance d’une mauvaise arme.",
            " forgeron concepteur, mécanicien",
            "1",

            "l’arme gagne 1 en point de rupture, et le cas échéant le renforcement supprime le malus de COU",
            "1 arme « de base » ou « perrave »",
            "",
            "1 heure",
            "enclume, marteau, nécessaire d’entretien du métal",
            "7(+3XP en cas de réussite)",
            "en tant que service, de 3 PO à 5 PO selon l’arme",
            "",
            "Pas de réalisation possible",
            "Équipement"),
        new Equipments(true,
            2,
            "Piège forestier à détente",
            "Utilisez une branche élastique comme ressort pour piéger quelqu’un. En forêt seulement. On s’en sert aussi pour attraper du gibier.",
            "mécanicien, cuisinier",
            "1",
            "en marchant dans un nœud coulant, la victime décolle et se fait pendre par un pied – aucun dégât – possibilité de se libérer avec une arme tranchante",
            "10 mètres de cordelette ou corde",
            "",
            "15 minutes",
            "canif ou couteau",
            "8(+2XP en cas de réussite)",
            "gratuit, non vendable",
            "",
            "-2",
            "Équipement"),
        new Equipments(true,
            2,
            "Piège à l'arbalète",
            "Utilisez une arbalète montée sur trépied pour blesser quelqu’un. Possible en extérieur comme en intérieur.",
            "mécanicien, gobelin",
            "1",
            "en déplaçant la ficelle de tension, la victime située dans le champ de tir se prend un carreau – dégâts selon arme",
            "1 unité de bois brut, 10 mètres de ficelle, 15 petits clous",
            "1 arbalète,  1 carreau",
            "20 minutes",
            "canif, marteau, scie à main",
            "10(+5XP en cas de réussite)",
            "gratuit, non vendable",
            "",
            "-4",
            "Équipement"),
            new Equipments(true,
                2,
                "Lance-pierre de base",
                "On peut difficilement parler d’arme, mais bon... C’est faisable",
                "mécanicien, gobelin, ébéniste",
                "1",
                "arme de jet, 2 P.I. contondants, rupture 1-5",
                "½ unité de bois brut, 6 petits clous",
                "1 caoutchouc de Glandorn (2 PO en bazar)",
                "20 minutes",
                "canif, marteau, râpe à bois",
                "6(+2XP en cas de réussite)",
                "de fabrication : 3 PO – non vendable",
                "",
                "-4",
                "Équipement"),

    ],

    lvl2: [
        new Equipments(true,
            4,
            "Arc long de basse qualité",
            "Une arme de jet de base, conçue pour débuter, difficile à manier.",
            "ébéniste, mécanicien",
            "1",
            "arme de jet, 1D+3 P.I., AD-2 (au tir de flèche), CHA-1, rupture 1-5",
            "1 unité de bois brut, ½ d’unité de cuir souple de base",
            "1 corde à arc (au bazar, 3 PO)",
            "3 heures",
            "scie à main, canif, râpe à bois",
            "8(+5XP en cas de réussite)",
            "de fabrication : 5 PO – de négoce : 15 PO – de vente au public : 50 PO",
            "",
            "-4",
            "Équipement")
    ],
    lvl3: [

    ],
    lvl4: [

    ],
    lvl5: [

    ]
}
// console.log(myRecipes.lvl1);
const classDescription = document.querySelector('#class-showing')
const hiddenClassDescription = document.querySelector("#hidden-description")

const speDescription = document.querySelector('#spe-showing')
const hiddenSpeDescription = document.querySelector("#hidden-spe")

const recipes = document.querySelector("#recipes")
const recipesLvl = document.querySelector('#recipes-lvl')
const titleLvl1 = document.querySelector("#lvl1-title")
const recipesLvl1 = document.querySelector('#lvl1')
const titleLvl2 = document.querySelector("#lvl2-title")
const recipesLvl2 = document.querySelector('#lvl2')
console.log(recipesLvl1);



const showing = (div, element) => {
    div.addEventListener('click', (e) => {
        e.preventDefault()
        element.classList.toggle('d-none')
        if (element === recipesLvl1 || element === recipesLvl2) {
            element.classList.toggle('d-flex')
        }


    })
}

const showRecipes = (elements, lvl, id) => {
    // elements c'est myRecipes 
    // lvl c'est myRecipes.lvl[id]
    // et id c'est les noms des clés de elements
    // console.log(elements.lvl1[0]);


    lvl.forEach((element, index) => {
        // console.log(element);
        if (element.known) {
            // console.log(id);
            let newDiv = document.createElement('div')
            let currentDiv = document.getElementById(`${Object.keys(elements)[id]}`)
            newDiv.setAttribute('id', `${Object.keys(elements)[id]}-${index + 1}`)
            newDiv.setAttribute('class', 'recipe-container col-2 mx-1 my-3')
            currentDiv.appendChild(newDiv)
            newDiv.innerHTML = `<p>
            <span>${element.name}</span> (${element.type})
            <br>${element.description}
            <br><span>Spécialités :</span> ${element.spec}
            <br><span>Niveau :</span> à partir du niveau ${element.lvl}
            <br>
            <br><span>Matériaux :</span> ${element.materials}
            <br><span class="optional">Matériaux spéciaux :</span> ${element.specMats}
            <br><span>Temps :</span> ${element.time}
            <br><span>Outils :</span> ${element.tools}
            <br><span>Difficulté :</span> ${element.difficult}
            <br>
            <br><span>Assistant :</span> ${element.assist}</span>
            <br><span>Malus pour le non-spécialiste :</span> ${element.malus}
            <span id="special-${index}"></span>
            </p>`
            if (element.type === "Équipement") {
                let toInsert = document.querySelector(`#${Object.keys(elements)[id]}-${index + 1} span:last-child`)
                toInsert.innerHTML += `<br><span>Caractéristiques :</span> ${element.carac}`
            }

            // console.log(lvl);



            // console.log(currentDiv);
        }
    });
}
showRecipes(myRecipes, myRecipes.lvl1, 0)
showRecipes(myRecipes, myRecipes.lvl2, 1)
showRecipes(myRecipes, myRecipes.lvl3, 2)
showRecipes(myRecipes, myRecipes.lvl4, 3)
showRecipes(myRecipes, myRecipes.lvl5, 4)
showing(classDescription, hiddenClassDescription)
showing(speDescription, hiddenSpeDescription)
showing(recipes, recipesLvl)
showing(titleLvl1, recipesLvl1)
showing(titleLvl2, recipesLvl2)